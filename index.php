<?php

// Задача №1. Составить массив, ключами в котором будут названия континентов (на английском - Africa), значениями — массивы из латинских названий зверей (например, Mammuthus columbi – можно найти в карточке статистики о животном справа). Найдите различных зверей и составьте массив так, чтобы для ключа Africa у вас значением был бы массив из зверей, там обитавших или обитающих. Выберите только один континент для каждого животного. Пусть у вас получится 10-15 зверей.

$continents = array(
	"Africa" => array("Mammuthus columbi", "Acinonyx jubatus", "Crocuta crocuta", "Gazella dorcas"),
	"Australia" => array("Macropus eugenii", "Phascolarctos cinereus", "Dromaius novaehollandiae", "Sarcophilus laniarius"),
	"South America" => array("Vicugna pacos", "Puma concolor", "Crocodylus intermedius", "Vultur gryphus")
);

// Задача №2. Теперь найдите всех зверей, название которых состоит из двух слов. Составьте из них новый массив.

$animals = array();

foreach ($continents as $key => $value) {
	foreach ($value as $intokey => $intovalue) {
		$doubleString = strpos($intovalue, " ");
		if (empty($doubleString)) continue;
		$animals[] = $intovalue;
	}
}

// Задача №3. Случайно перемешайте между собой первые и вторые слова названий животных так, чтобы на выходе мы получили выдуманных, фантазийных животных. Важно, чтобы каждый кусочек был использован и не повторялся более одного раза. Ничего страшного, если в результате перемешивания иногда будут получаться реальные животные. Вывести этих животных на экран.

$firstPartAnimals = array(); // Массив первых частей названий животных.
$secondPartAnimals = array(); // Массив вторых частей названий животных.

foreach ($animals as $key => $value) {
	$firstPart = substr($value, 0, strpos($value, " "));
	$secondPart = substr($value, strpos($value, " "));
	$firstPartAnimals[] = $firstPart;
	$secondPartAnimals[] = $secondPart;
}

shuffle($secondPartAnimals); // Перемешиваем вторую часть слов.

$fantasticAnimals = array();

foreach ($firstPartAnimals as $key => $value) {
	$fantasticAnimals[] = $value.$secondPartAnimals[$key];
}

foreach ($fantasticAnimals as $key => $value) {
	echo "$key => $value <br>";
}

// Задача №4 и 5. Сохраните названия регионов, к которым относятся ваши звери. Принадлежность определяйте по изначальной принаждлежности первого кусочка названия животного. Пусть названия регионов выводятся заголовками <h2>, а под ними – относящиеся к этому региону животные. Между животными выводите запятые. В конце не должно быть висящих запятых, неуместных по правилам пунктуации.

$continentsKeys = array_keys($continents); // Для задачи №4. Далее нафиг не нужно.

for (reset($continents); ($key = key($continents)); next($continents)) { 
	echo "<h2>$key:</h2>";
	foreach ($fantasticAnimals as $key2 => $value2) {
		$partFantasticAnimals = substr($value2, 0, strpos($value2, " "));
		foreach ($continents[$key] as $key3 => $value3) {
			$partContinents = substr($value3, 0, strpos($value3, " "));
			if ($partFantasticAnimals === $partContinents) {
				echo $value2;
				if ($value3 === end($continents[$key])) {
					echo ".<br>";
				} else {
					echo ", ";
				}
			}
		}
	}
}